USE CSCI4823finalprojectdb;

CREATE TABLE hepatitis
(
age int,
sex varchar(15),
steroid varchar(5),
antivirals varchar(5),
fatigue varchar(5),
malaise varchar(5),
anorexia varchar(5),
liver_big varchar(5),
liver_firm varchar(5),
spleen_palpable varchar(5),
spiders varchar(5),
ascites varchar(5),
varices varchar(5),
bilirubin double,
alk_phosphate int,
sgot int,
albumin double,
protime int,
histology varchar(5),
class varchar(10)
);



