# Data Mining with MySQL and WEKA

For this project, I have taken 4 data sets and extracted data for analysis using MySQL Workbench and WEKA. 

## Included in this project:
* The sql files that created the tables for each dataset to connect into WEKA.
* The datasets I used in arff file format.
* A word document with screenshots and more detail on what I extracted from the datasets with WEKA.

## Data Mining techniques used in order:
* Apriori Method used for the all_grades dataset about student grades compared with parent satisfaction and involvement.
* Clustering (k means) used for the hepatitis dataset to show commonalities and differences between people with hepatitis.
* Decision tree (j48) used for the rainfall dataset to see how and if rainfall can be predicted.
* Outlier Detection used for the housing dataset that shows how different demographics effect student/teacher ratio, crime rates, zoning, etc.