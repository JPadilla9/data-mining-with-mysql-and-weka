use csci4823finalprojectdb;
create table rainprediction
(
datetaken varchar(15) primary key,
location varchar(30),
mintemp double,
maxtemp double,
rainfall double,
windgust varchar(10),
windgustspeed int,
wind_dir_9am varchar(10),
wind_dir_3pm varchar(10),allgrades
wind_speed_9am int,
wind_speed_3pm int,
humidity_9am int,
humidity_3pm int,
pressure9am double,
pressure3pm double,
cloud9am double,
cloud3pm double,
temp9am double,
temp3pm double,
rainToday varchar(10),
rainTomorrow varchar(10)
);

