USE csci4823finalprojectdb;
CREATE TABLE allgrades
(
gender varchar(5),
nationality varchar(30),
place_of_birth varchar(30),
stage_id varchar(30),
grade_id varchar(30),
section_id varchar(5),
topic varchar(15),
semester varchar(5),
relation varchar(15),
raised_hands int,
visited_resources int,
annoucement_view int,
discussion int,
parent_answering_survey varchar(15),
parent_school_satisfaction varchar(15),
student_absent_days varchar(30),
class varchar(10)
);
